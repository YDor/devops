
=============================

To run docker containers:
------------

    * install VirtualBox (https://www.virtualbox.org/)
    * install vagrant (https://www.vagrantup.com/)
    * install vagrant-persistent-storage (https://github.com/kusnier/vagrant-persistent-storage):
        vagrant plugin install vagrant-persistent-storage
    * start devBox VM:
        workspace\devops\vagrant> vagrant up
    * log into devBox VM:
        workspace\devops\vagrant> vagrant ssh
    * use docker:
        vagrant@devBox:~$ docker ps



install with Chocolatey(https://chocolatey.org/) for windows is easy:
------------

    * cmd (as admin)
    * @powershell -NoProfile -ExecutionPolicy Bypass -Command "[System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH="%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
    * choco install virtualbox
    * choco install vagrant
